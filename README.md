# HAAR与DLib的实时人脸检测

对于opencv的人脸检测方法，优点是简单，快速；存在的问题是人脸检测效果不好。正面/垂直/光线较好的人脸，该方法可以检测出来，而侧面/歪斜/光线不好的人脸，无法检测。因此，该方法不适合现场应用。而对于dlib人脸检测方法采用64个特征点检测，效果会好于opencv的方法识别率会更高。

至于哪种方法更好，写个代码就知道。



## 安装

本示例的代码是基于Python3。

1. 安装Python3的虚环境:

```
$ virtualenv venv -p python3
```


2. 安装基本依赖

激活虚环境然后执行以下的代码安装本示例所用到的全部依赖包

```
$ pip install -r requirements.txt
```

## 示例说明

- [cv_base.py](cv_base.py)   - 用OpenCV开启实时视频
- [cv_harr.py](cv_harr.py)   - 用HAAR分类器进行人脸检测
- [cv_dlib.py](cv_dlib.py)   - 用Dlib进行人脸检测
- [landmark.py](landmark.py) - 用Dlib的特征提取器追踪人脸的64个特征点


